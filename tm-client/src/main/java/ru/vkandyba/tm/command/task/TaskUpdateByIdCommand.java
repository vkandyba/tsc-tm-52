package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.endpoint.Task;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Update task by id...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("Enter Id");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().updateTaskById(session, id, name, description);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
