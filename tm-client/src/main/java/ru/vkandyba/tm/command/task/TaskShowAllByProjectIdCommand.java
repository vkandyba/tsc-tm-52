package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskShowAllByProjectIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "show-all-tasks-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();

        System.out.println(serviceLocator.getTaskEndpoint().showAllTasksByProjectId(session, projectId));
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
