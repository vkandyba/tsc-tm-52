package ru.vkandyba.tm.command;

import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public Role[] roles() {
        return null;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
