package ru.vkandyba.tm.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import ru.vkandyba.tm.listener.LoggerListener;
import ru.vkandyba.tm.service.ReceiverService;

public class Bootstrap {

    public void init(){
        final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        final ReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }
}
