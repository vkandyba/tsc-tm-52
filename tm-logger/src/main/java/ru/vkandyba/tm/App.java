package ru.vkandyba.tm;

import ru.vkandyba.tm.bootstrap.Bootstrap;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
