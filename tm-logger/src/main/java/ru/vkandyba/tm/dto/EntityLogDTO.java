package ru.vkandyba.tm.dto;

import lombok.Getter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@XmlRootElement
public class EntityLogDTO implements Serializable {

    private final String className;

    private final String date;

    private final String entity;

    private final String id = UUID.randomUUID().toString();

    private final String type;

    public EntityLogDTO(String className, String date, String entity, String type) {
        this.className = className;
        this.date = date;
        this.entity = entity;
        this.type = type;
    }


}