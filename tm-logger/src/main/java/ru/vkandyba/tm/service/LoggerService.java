package ru.vkandyba.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import ru.vkandyba.tm.dto.EntityLogDTO;

import javax.jms.ObjectMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;

public class LoggerService {
    ObjectMapper objectMapper = new ObjectMapper();
    MongoClient mongoClient = new MongoClient("localhost", 27017);
    MongoDatabase db = mongoClient.getDatabase("logger");

    @SneakyThrows
    public void writeLog(final EntityLogDTO message){
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
        final LinkedHashMap event = objectMapper.readValue(json, LinkedHashMap.class);
        db.createCollection(message.getClassName());
        MongoCollection<Document> collection = db.getCollection(message.getClassName());
        System.out.println(json);
        collection.insertOne(new Document(event));
    }



}
