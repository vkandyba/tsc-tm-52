package ru.vkandyba.tm.constant;

public class TableConst {

    public static String PROJECT_TABLE = "app_projects";

    public static String TASK_TABLE = "app_tasks";

    public static String SESSION_TABLE = "app_sessions";

    public static String USER_TABLE = "app_users";

}
