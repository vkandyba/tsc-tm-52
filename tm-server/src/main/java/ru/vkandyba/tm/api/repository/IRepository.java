package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.dto.AbstractEntityDTO;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntityDTO> {

    Boolean existsByIndex(Integer index);

    Boolean existsById(String id);

    E findById(String id);

    E findByIndex(Integer index);

    void removeById(String id);

    void removeByIndex(Integer index);

    void add(E entity);

    void remove(E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void addAll(List<E> entities);

    void clear();

}
