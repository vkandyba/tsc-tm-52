package ru.vkandyba.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskService extends AbstractService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public List<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll(userId);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public Task findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public Task findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByName(userId, id);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void add(String userId, Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeByName(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.updateById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.startById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.startByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.finishById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.finishByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
