package ru.vkandyba.tm.dto;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.listener.EntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@Table(name = "app_sessions")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionDTO extends AbstractEntityDTO implements Cloneable{

    public SessionDTO() {
    }

    @Override
    public SessionDTO clone() throws CloneNotSupportedException {
        return (SessionDTO) super.clone();
    }

    @Column
    @Nullable
    private Long timestamp;

    @Column(name = "user_id")
    @Nullable
    private String userId;

    @Column
    @Nullable
    private String signature;

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
