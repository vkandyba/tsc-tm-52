package ru.vkandyba.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.listener.EntityListener;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_tasks")
@EntityListeners(EntityListener.class)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO extends AbstractBusinessEntityDTO {

    @Column
    private String name;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private Status status = Status.NON_STARTED;

    @Column(name = "project_id")
    private String projectId = null;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "finish_date")
    private Date finishDate;

    @Column(name = "created_date")
    private Date createdDate = new Date();

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
