package ru.vkandyba.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public List<Task> findAll(@NotNull String userId){
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public Task findById(@NotNull String userId, @NotNull String id){
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.userId = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public Task findByName(@NotNull String userId, @NotNull String name){
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.userId = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByName(@NotNull String userId, @NotNull String name){
        Task task = findByName(userId, name);
        remove(task);
    }

    public void startById(@NotNull String userId, @NotNull String id){
        Task task = findById(userId, id);
        task.setStatus(Status.IN_PROGRESS);
        update(task);
    }

    public void startByName(@NotNull String userId, @NotNull String name){
        Task task = findByName(userId, name);
        task.setStatus(Status.IN_PROGRESS);
        update(task);
    }

    public void finishById(@NotNull String userId, @NotNull String id){
        Task task = findById(userId, id);
        task.setStatus(Status.COMPLETED);
        update(task);
    }

    public void finishByName(@NotNull String userId, @NotNull String name){
        Task task = findByName(userId, name);
        task.setStatus(Status.COMPLETED);
        update(task);
    }

    public void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description){
        Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        update(task);
    }

    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId){
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.userId = :userId AND e.projectId = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId){
        entityManager.createQuery("DELETE FROM Task e WHERE e.userId = :userId AND e.projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    public void clear(@NotNull String userId){
        entityManager.createQuery("DELETE FROM Task e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
