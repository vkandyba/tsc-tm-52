package ru.vkandyba.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.AbstractEntityDTO;
import ru.vkandyba.tm.model.AbstractEntity;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable E entity){
        if(entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable E entity){
        if(entity == null) return;
        entityManager.merge(entity);
    }

    public void remove(@Nullable E entity){
        if(entity == null) return;
        entityManager.remove(entity);
    }

}
