package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IProjectEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.ProjectDTO;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllProjects(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getProjectDtoService().findAll(sessionDTO.getUserId());
    }

    @Override
    public void addProject(@NotNull SessionDTO sessionDTO, @NotNull ProjectDTO project) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().add(sessionDTO.getUserId(),project);
    }

    @Override
    public void finishProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().finishById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void finishProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().finishByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void removeProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().removeById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void removeProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    public ProjectDTO showProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getProjectDtoService().findById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void startProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().startById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void startProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().startByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void updateProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().updateById(sessionDTO.getUserId(), projectId, name, description);
    }

    @Override
    public void removeProjectWithTasksById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().removeById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void clearProjects(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getProjectDtoService().clear(sessionDTO.getUserId());
    }

}
